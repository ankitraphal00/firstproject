

function sendsuccess(res, data){
    res.status(200).send(data);
}
function senderror(res, error){
    res.status(400).send({
        error:error
    })
}

module.exports={
    sendsuccess:sendsuccess,
    senderror:senderror
}