const express = require('express');
const usercontroller = require('../controllers/usercontroller');
const auth = require('../middleware/authmiddleware');
const router = new express.Router();

router.get('/users', usercontroller.getAllUsers);
router.get('/users/testlean/:id', usercontroller.testinglean);
router.post('/users', usercontroller.createUser);
router.put('/users/testupsert/:id', usercontroller.testingupsert);
router.delete('/users/:id', usercontroller.deleteUser);
router.post('/users/signup', usercontroller.signup);
router.put('/users/:id', usercontroller.updateUser);
router.post('/users/login', usercontroller.userlogin);
router.get('/users/profile', auth,usercontroller.Profile)
router.post('/users/logout', auth, usercontroller.logOut)
module.exports = router;
