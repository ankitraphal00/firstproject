const User = require("../models/usersmodel");

module.exports.saveUser = async (name, email, age) => {
  const user = await new User({
    name,
    email,
    age,
    password
  });

  await user.save();
};

// module.exports.getall = async () => {
//   try {
//     const user = await User.find({});
//     console.log("user find");
//     if (!user) {
//       console.log("error");
//       throw new Error("Not able to find any user");
//     }
//     return user;
//   } catch (error) {
//     console.log("Error in finding all users", error);
//   }
// };

module.exports.getall = async () => {
  const user = await User.find({});
  if (!user) {
    console.log("error");
    throw new Error("Not able to find any user");
  }
  return user;
};

module.exports.deleteuser = async (id) => {
  const user = await User.findById(id);
  if (!user) {
    throw new Error("user not found");
  }
  // const us = user;
  await user.remove();
  return user;
  // return us;  
  // return user;

};

// module.exports.updateuser = async (id, name, email, age) => {
//   try {
//     const user = await User.findById(id);
//     if (!user) {
//       console.log("error in updating the user");
//       throw new Error("User not find");
//     }
//     user.name = name;
//     user.email = email;
//     user.age = age;

//     await user.save();
//   } catch (error) {
//     console.log("update user error", error);
//   }
// };

//
module.exports.updateuser = async (user_id, body) => {
  const updates = Object.keys(body);
  const allowedUpdates = ["name", "age", "email"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );
  try {
    if (!isValidOperation) {
      throw new Error("Invalid update operation");
    }
    const user = await User.findById(user_id);
    updates.forEach((update) => (user[update] = body[update]));
    await user.save();
    return user;
  } catch (error) {
    return console.log("Error Updating user \n" + error);
  }
};
//
module.exports.testingUpsert = async (id, update) => {
  const data = await User.findOneAndUpdate(id, update, { upsert: true }, function (err, res) {
    if (err)
      throw new error({ error: 'Note cannot be updated' })
    return res;
  });
}


module.exports.testinglean = async (id) => {

  const user = await User.findOne({ user_id: id }).lean();
  user.name = "testing";

  return user;
}

module.exports.signUp = async (body) => {

  const user = await new User(body)
  await user.save()
  // const token = await user.generateAuthToken()
  // return { user, token }
  return user;


}

module.exports.userLogin = async (email, password) => {

  const user = await User.findUserByCredes(email, password);
  const token = await user.generateAuthToken()
  return { user, token };



}

module.exports.profile = async (user) => {
  return user;
}

module.exports.logout = async (req) => {

  req.user.tokens = req.user.tokens.filter((token) => {
    return token.token !== req.token
  })
  await req.user.save()
  return req.user
}
