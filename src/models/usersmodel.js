const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const bcrypt =require('bcrypt');
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true,

    },
    email: {
        type: String,
        require: true,
        unique: true,
        trim: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Invalid Email");
            }
        },

    },
    age:
    {
        type: Number,
        require: true,
        default: 0,
        validate(value) {
            if (value < 0) {
                throw new Error("Invalid age");
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true

    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }
    ]

}, { timestamps: true })

// userSchema.virtual('domain').get(function () {
//     return this.email.slice(this.email.indexOf('@') + 1);
// })

userSchema.statics.findUserByCredes = async (email, password) => {
    const user = await User.findOne({ email: email});
    // console.log("inside function");
    // console.log(user);
   
    if (!user) {
        throw new Error("User not found");
    }
    const match = await bcrypt.compare(password, user.password)
    if (!match) {
        throw new Error("Unable to logIn")
    }
    return user

 
}

userSchema.methods.generateAuthToken = async function () {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, 'hello');

    user.tokens = user.tokens.concat({ token });
    await user.save();
    return token;
}
userSchema.pre('save', async function (next) {

    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 10)
    }
    next()
})

const User = mongoose.model("user", userSchema);


module.exports = User;