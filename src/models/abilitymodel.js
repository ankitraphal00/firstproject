const mongoose = require("mongoose");
const abilitiesSchema = new mongoose.Schema({
    speed:{
        type:Number,   
        required:true,
    },
    level:{
        type:String,
        required:true,
    },
    usedfor:{
        type:String,
        required:true
    }
})

const Abilities = mongoose.model("abilities",abilitiesSchema);
module.exports = Abilities;
