const jwt = require('jsonwebtoken')
const User = require('../models/usersmodel')
const common = require('../utility/common');

const auth = async (req, res, next) => {
    try {

        const token = req.header('Authorization').replace('Bearer ', '')
        const decodedToken = await jwt.verify(token, 'hello')

        const user = await User.findOne({ _id: decodedToken._id, 'tokens.token': token })
        if (!user) {
            throw new Error()
        }
        req.token = token
        req.user = user

        next()

    } catch (error) {
        common.senderror(res, 'Please Authenticate!')
    }
}
module.exports = auth
