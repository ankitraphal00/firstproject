const User = require("../models/usersmodel");
const userServices = require("../services/userServices");
const common = require("../utility/common");

module.exports.getAllUsers = async (req, res) => {
  try {
    const ans = await userServices.getall();
    common.sendsuccess(res, ans);
  } catch (error) {
    common.senderror(res, error.message);
  }
};

module.exports.createUser = async (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const age = req.body.age;

  try {
    if (!name || !email || !age) {
      console.log("Error in attributes");

      return common.senderror(res, "all attributes are not defined proprely ");
    }
    await userServices.saveUser(name, email, age);
    res.send("user created");
  } catch (error) {
    console.log("error");

    common.senderror(res, error.message);
  }
};

module.exports.deleteUser = async (req, res) => {
  try {
    const user = await userServices.deleteuser(req.params.id);

    if (!user) {
      return common.senderror(res, "User not found");
    }
    common.sendsuccess(res, "User Deleted");
  } catch (error) {
    common.senderror(res, "Unable to delete the user");
  }
};

module.exports.testingupsert = async (req, res) => {
  try {
    const result = await userServices.testingUpsert(req.params.id, req.body);
    console.log(result);
    common.sendsuccess(res, "note updated");
  } catch (error) {
    common.senderror(res, error.message);
  }
};
// module.exports.updateUser = async (req, res) => {
//   const user = await userServices.updateuser(req.params.id, req.body);
//   res.send("user updated");
// };

module.exports.updateUser = async (req, res) => {

  try {

      if (req.user.id !== req.params.id) {
          throw new Error('Invalid request')
      }
      const user = await userServiceJWT.updateuser(req.params.id, req.body)
      common.sendSuccess(res, user)

  } catch (error) {
      common.sendError(res, error.message)
  }
}

module.exports.testinglean = async (req, res) => {
  const result = await userServices.testinglean(req.params.id);
  common.sendsuccess(res, "userfound");
};

module.exports.signup = async (req, res) => {
  // const name = req.body.name;
  // const email = req.body.email;
  // const age = req.body.age;
  // const password = req.body.password;
  // console.log("This is the password :", password);
  // try {
  //   if (!name || !email || !age || !password) {
  //     console.log("Error in attributes");

  //     return common.senderror(res, "all attributes are not defined proprely ");
  //   }
  //   await userServices.signUp(name, email, age, password);
  //   res.send("user created");
  // } catch (error) {
  //   console.log("error");
  //   console.log("This is the password :", password);
  //   common.senderror(res, error.message);
  // }

  try {
    const user = await userServices.signUp(req.body);
    common.sendsuccess(res, user);
  } catch (err) {
    common.senderror(res, err.message);
  }

}

module.exports.userlogin = async (req, res) => {
  try {
    const user = await userServices.userLogin(req.body.email, req.body.password);
  
    common.sendsuccess(res, user);
  } catch (err) {
    common.senderror(res, err.message);
  }
}

module.exports.Profile = async (req, res) => {

  try {
    const user = await userServices.profile(req.user);
    common.sendsuccess(res, user);
  } catch (err) {
    common.senderror(res, err.message);
  }
}


module.exports.logOut = async (req, res) => {

  try {
    const user = await userServices.logout(req)
    common.sendsuccess(res, user)
  } catch (err) {
    common.senderror(res, err.message)
  }

}

// module.exports.testing